const employeeData = require("../js_drill_2.cjs");
const sumOfSalaryProblem = require("../problem4.js");
const salaryIntoNumber = require("../problem2.js"); //to convert salary into number function
const convertedSalary = salaryIntoNumber(employeeData);

try {
  console.log(sumOfSalaryProblem(convertedSalary));
} catch (error) {
  console.log("There is some error in code");
}
