const employeeData = require("./js_drill_2.cjs");

// Find the average salary of based on country. ( Group it based on country and then find the average ).

function getAverageSalary(employeeData) {
  if (Array.isArray(employeeData)) {
    const averageSalaryBasedOnCountry = employeeData.reduce(function (
      accum,
      curr
    ) {
      let count = 0;
      let salaryIntoNumber = parseFloat(curr.salary.replace("$", ""));
      if (!accum[curr.location]) {
        accum[curr.location] = {
          totalSalary: salaryIntoNumber,
          employeeCount: 1,
        };
      } else {
        count++;
        accum[curr.location].totalSalary = +salaryIntoNumber;
        accum[curr.location].employeeCount++;
      }
      return accum;
    },
    {});

    for (let country in averageSalaryBasedOnCountry) {
      //for-in loop takes keys of objects
      averageSalaryBasedOnCountry[country].averageSalary =
        averageSalaryBasedOnCountry[country].totalSalary /
        averageSalaryBasedOnCountry[country].employeeCount;
      // delete averageSalaryBasedOnCountry[country].employeeCount;
      delete averageSalaryBasedOnCountry[country].totalSalary;
    }

    return averageSalaryBasedOnCountry;
  } else {
    return null;
  }
}

module.exports = getAverageSalary;
