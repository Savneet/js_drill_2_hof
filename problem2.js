const employeeData = require("./js_drill_2.cjs");

// 2. Convert all the salary values into proper numbers instead of strings.


function convertSalaryIntoNumber(employeeData)
{
  if (Array.isArray(employeeData)) {
    const salaryIntoNumber = employeeData.map(function (employeeInfo) {
      return (employeeInfo.salary = parseFloat(
        employeeInfo.salary.replace("$", "")
      ));
    });
    return salaryIntoNumber;
  } else {
    return null;
  }
}

module.exports = convertSalaryIntoNumber;