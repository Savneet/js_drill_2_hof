const employeeData = require("./js_drill_2.cjs");

// Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function findSumOfSalaryBasedOnCountry(employeeData)
{
  if (Array.isArray(employeeData)) {
  
    const sumOfSalaryBasedOnCountry=employeeData.reduce(function(accum,curr){
        
        let salaryIntoNumber=parseFloat(curr.salary.replace("$",""));
        if(!accum[curr.location])
        {
            accum[curr.location]=salaryIntoNumber;
        }
        else{
            accum[curr.location]+=salaryIntoNumber;
        }
        return accum;
    },{});
  
    return sumOfSalaryBasedOnCountry;
    
} else {
  return null;
}

}
module.exports=findSumOfSalaryBasedOnCountry;