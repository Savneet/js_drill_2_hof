const employeeData = require("./js_drill_2.cjs");

// 4. Find the sum of all salaries.

function findSumOfSalary(salaryIntoNumber) {
  if (Array.isArray(salaryIntoNumber)) {
    const sumOfSalary = salaryIntoNumber.reduce(function (accum, curr) {
      accum = accum + curr;
      return accum;
    });

    return sumOfSalary;
  } else {
    return null;
  }
}

module.exports = findSumOfSalary;
