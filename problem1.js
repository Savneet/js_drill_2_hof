const employeeData = require("./js_drill_2.cjs");

//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

function findWebDeveloperProblem(employeeData) {
  if (Array.isArray(employeeData)) {
    const findWebDeveloper = employeeData.filter(function (employeeInfo) {
      if (employeeInfo.job.indexOf("Web") == 0) {
        return true;
      }
    });

    return findWebDeveloper;
  } else {
    return null;
  }
}
module.exports = findWebDeveloperProblem;
