const employeeData = require("./js_drill_2.cjs");

// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function findCorrectedSalary(employeeData)
{
  if (Array.isArray(employeeData)) {
    const correctedNewSalary = employeeData.forEach(function (employeeInfo) {
      employeeInfo.corrected_salary =
        parseFloat(employeeInfo.salary.replace("$", "")) * 10000;
    });
    return employeeData;
  } else {
    return null;
  }  
}
module.exports = findCorrectedSalary;
